const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const pantrySchema = new Schema({
    pantryName: { type: String, unique: true, required: true },
    ingredients:Array,
    dateCreated: String,
})
module.exports =  mongoose.model('pantry', pantrySchema);