const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const recipesSchema = new Schema({
    category: { type: String, unique: false, required: true },
    recipeTitle:{ type: String, unique: true, required: true },
    image: String,
    cookingTime: String,
    ingredients: Array,
    digest: Array,
    nutritionLabels: String,
    recipeLink: String, 
    dateCreated: String,
    recipeSource: String,
    totalTime: String,
})
module.exports =  mongoose.model('recipes', recipesSchema);