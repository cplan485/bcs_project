const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/pantry.controller');


//  == This route allow us to add an extr category: ==  //

router.post('/add', controller.insert);

//  == This route allow us to delete one category t will be that with the id we are providing: ==  //

router.post('/delete', controller.delete);

//  == This route allow us to update ingredients or pantry name ==  //

router.post('/update', controller.update);

//  This route will give us back all pantries with associated user email: ==  //

router.get('/:email', controller.findAll);

//  == This route will give us back one pantry that matches pantry_name: ==  //

router.get('/',controller.allPantries);

router.get('/one', controller.findOne);


module.exports = router;