const User = require('../models/users.models');
const Recipe = require('../models/recipes.model');

class RecipesController {
    // GET FIND ALL - THIS IS GOOD
    async findAll(req, res){
        let {email} = req.params;
        try{
            const matchingEmails = await User.find({email});
            //console.log(`Match Email23432`,matchingEmails.length)
            if(matchingEmails.length>0) {
                let RecipesArr ={};
                matchingEmails.forEach(obj => RecipesArr.recipes = obj.recipes)
                
                console.log(`Recipes Baby`, RecipesArr)
            res.send(RecipesArr);
    
             }
             else {
                 res.send(`User with Recipes Email: ${email} doesn't exist!`)
         }
        }
        catch(e){
            res.send({e})
        }
    }
    //NEW FUNCTION DISPLAY ALL CATEGORIES
    async findCategories(req, res){
        try{
            let allCategories = [];
            const all = await Recipe.find({});
            all.forEach( (obj,idx) => {
            allCategories.push( {[idx]: obj.category})
            })
            res.send(allCategories);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE PANTRY WITHIN A USER**
    async findOne(req ,res){
        //_id
        let {email} = req.body;
        try{
            const matchingEmails = await User.find({email});
           
            if(matchingEmails.length>0) {
                const UserPantries = matchingEmails.pantries
               // console.log(`Tenacious D`, UserPantries)
            res.send(UserPantries);
    
             }
             else {
                 res.send(`User with Recipe  Email: ${email} doesn't exist!`)
         }
        }
        catch(e){
            res.send({e})
        }

    }

    
  // POST ADD ONE -- COMPLETED 12/29/2012
  async insert (req, res) {
    let {category, ingredients, recipeTitle, image, recipeSource, cookingTime, nutritionLabels,recipeLink, digest, dateCreated, email} = req.body;
    try{
        const newRecipe = await Recipe.create({recipeTitle: recipeTitle,
             category: category,
             image: image,
             cookingTime: cookingTime,
             ingredients: ingredients,
             nutritionLabels: nutritionLabels,
             recipeLink: recipeLink,
             recipeSource: recipeSource,
             digest: digest, 
             dateCreated: dateCreated});
        // New code from 
       // const updated = await Category.updateOne({category},{$push: {products:{name: productName,price} } })
        const updated = await User.updateOne({email},{$push: {recipes:newRecipe } })
        const matchingEmails = await User.find({email});
        if(matchingEmails.length>0) {
        res.send(updated);

         }
         else {
             res.send(`User with Email: ${email} doesn't exist!`)
     }
    }
    catch(e){
        res.send({e})
    }
}

    // DELETE PANTRY
    async delete (req, res){
        let { recipeTitle,email} = req.body;
        try{
           const matchingEmails = await User.findOne({email});
            console.log(`Check MatchEMAIL`, matchingEmails.recipes)
            if(matchingEmails.recipes.length>0) {

            //console.log(`All Matched Emails ==>`, matchingEmails[0].pantries);
            const updatedArray = matchingEmails.recipes
            matchingEmails.recipes.forEach( (obj,idx) => {
                console.log(`Recipe Titles`,obj.recipeTitle)
                if (obj.recipeTitle === recipeTitle) {
                   updatedArray.splice(idx, 1);
                }
            })

            const updated = await User.updateOne(
                { email },{ recipes: updatedArray }
             );
            res.send({updated});
            //const removed = await Product.deleteOne({ name: productName });
            //res.send({removed});
            res.send(`debugging`)
        }
        else {
            res.send(`User or pantry name doesn't exist!`)
        }
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE PANTRY

    async update (req, res){
        let { email, pantryName, newPantryName, newIngredients, newDateCreated } = req.body;
        try{
            const matchingEmails = await User.find({email});
            if(matchingEmails.length>0) {
                const updatedArray = matchingEmails[0].pantries
                updatedArray.forEach( (obj, idx)=> {
                    if (obj.pantryName === pantryName) {
                        obj.pantryName = newPantryName;
                        obj.ingredients = newIngredients;
                        obj.dateCreated = newDateCreated;
                    }
                } )
            const updated = await User.updateOne(
                 { email },{ pantries: updatedArray }
             );
            res.send({updated});
            }
            else {
                res.send(`PantryName doesn't exist!`)
            }
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new RecipesController();