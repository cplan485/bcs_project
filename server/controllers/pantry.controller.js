const User = require('../models/users.models');
const Pantry = require('../models/pantry.model');

class PantryController {
    // GET FIND ALL - THIS IS GOOD

    async allPantries(req, res){
        
        try{
            const all = await Pantry.find({});
       
            res.send(all);
    
        }
        catch(e){
            res.send({e})
        }
    }
    async findAll(req, res){
        let {email} = req.params;
        try{
            const matchingEmails = await User.find({email});
            //console.log(`Matching Email`,matchingEmails.length)
            if(matchingEmails.length>0) {
                let PantryArr ={};
                matchingEmails.forEach(obj => PantryArr.pantries = obj.pantries)
                const UserPantries = matchingEmails.pantries
                //console.log(`Array of Pantries`, PantryArr)
            res.send(PantryArr);
    
             }
             else {
                 res.send(`User with Pantry Email: ${email} doesn't exist!`)
         }
        }
        catch(e){
            res.send({e})
        }
    }
    //NEW FUNCTION DISPLAY ALL CATEGORIES
    async findCategories(req, res){
        try{
            let allCategories = [];
            const all = await Pantry.find({});
            all.forEach( (obj,idx) => {
            allCategories.push( {[idx]: obj.category})
            })
            res.send(allCategories);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE PANTRY WITHIN A USER**
    async findOne(req ,res){
        //_id
        let {email} = req.body;
        try{
            const matchingEmails = await User.find({email});
           
            if(matchingEmails.length>0) {
                const UserPantries = matchingEmails.pantries
                //console.log(`Matching Pantry`, UserPantries)
            res.send(UserPantries);
    
             }
             else {
                 res.send(`User with Pantry Email: ${email} doesn't exist!`)
         }
        }
        catch(e){
            res.send({e})
        }

    }

    
  // POST ADD ONE -- COMPLETED 12/18/2012
  async insert (req, res) {
    let {pantryName, ingredients, dateCreated, email} = req.body;
    try{
        const newPantry = await Pantry.create({pantryName:pantryName,
             ingredients: ingredients,
             dateCreated: dateCreated});
        // New code from 
       // const updated = await Category.updateOne({category},{$push: {products:{name: productName,price} } })
        const updated = await User.updateOne({email},{$push: {pantries:newPantry } })
        const matchingEmails = await User.find({email});
        if(matchingEmails.length>0) {
        res.send(updated);

         }
         else {
             res.send(`User with Email: ${email} doesn't exist!`)
     }
    }
    catch(e){
        res.send({e})
    }
}

    // DELETE PANTRY
    async delete (req, res){
        let { pantryName,email} = req.body;
        try{
           const matchingEmails = await User.findOne({email});
            if(matchingEmails.pantries.length>0) {

            //console.log(`All Matched Emails ==>`, matchingEmails[0].pantries);
            const updatedArray = matchingEmails.pantries
            matchingEmails.pantries.forEach( (obj,idx) => {
               // console.log(`Pantry Name from pantry Controller`,obj.pantryName)
                if (obj.pantryName === pantryName) {
                   updatedArray.splice(idx, 1);
                }
            })

            const updated = await User.updateOne(
                { email },{ pantries: updatedArray }
             );
            res.send({updated});
            res.send(`debugging`)
        }
        else {
            res.send(`User or pantry name doesn't exist!`)
        }
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE PANTRY

    async update (req, res){
        let { email, pantryName, newPantryName, newIngredients, newDateCreated } = req.body;
        try{
            const matchingEmails = await User.find({email});
            if(matchingEmails.length>0) {
                const updatedArray = matchingEmails[0].pantries
                updatedArray.forEach( (obj, idx)=> {
                    if (obj.pantryName === pantryName) {
                        obj.pantryName = newPantryName;
                        obj.ingredients = newIngredients;
                        obj.dateCreated = newDateCreated;
                    }
                } )
            const updated = await User.updateOne(
                 { email },{ pantries: updatedArray }
             );
            res.send({updated});
            }
            else {
                res.send(`PantryName doesn't exist!`)
            }
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new PantryController();