## RECIPES DB

Create a products DB that stores our products and divides them by categories.
You should be able to create a CRUD (Create, Read, Update, Delete) app for adding categories as well as products. 
all products should have at least name , price , color and description, they should be editables.
all data must be passed through the body using postman instead of params through the url.

// ## API:

// | Method | URL                 | Action                                     |
// | ------ | ------------------- | ------------------------------------------ |
// | POST   | /category/add       | Add a  new category                        |
// | POST   | /category/delete    | Remove category                            |
// | POST   | /category/update    | Update category                            |
// | GET    | /category/categories| Display all categories                     |
// | GET    | /category/products  | display all categories with all products   |
// | GET    | /category/:category | display one category with all its products |
// | POST   | /product/add        | Add product                                |
// | POST   | /product/delete     | Delete product                             |
// | POST   | /product/update     | Update name, price, color or description   |

## API:
| Method | URL                      | Action                                                |
| ------ | -------------------      | ------------------------------------------            |
//USERS
| POST   | /users/add               | Add a  new user                                       |
| POST   | /users/delete            | Remove user                                           |
| POST   | /users/update            | Update user password or username                      |
| GET    | /users/                  | Display all users                                     |
| GET    | /users/:email             | display specified user with recipesDB                |
//PANTRY
| POST   | /pantries/add              | Add a  new pantry                                   |
| POST   | /pantries/delete           | remove pantry                                       |
| POST   | /pantries/update           | Update ingredients or pantry_name                   |
| GET    | /pantries/                 | Display all pantries of specified user              |
| GET    | /pantries/:pantry_name     | display pantry by pantry_name                       |
//RECIPES LIST
| GET    | /recipesli/              | show all collection of recipes and categories         |
| GET    | /recipesli/categories    | show all recipe list categories                       |
| GET    | /recipesli/:category     | show all collection of recipes that match category    |
| POST   | /recipesli/delete        | delete all recipe lists matching specified category   |
| POST   | /recipesli/add           | add new category with recipe list                     |
| POST   | /recipesli/update        | change category of existing recipelist                |
//RECIPES
| GET    | /recipes/                | show all recipes of user                              |
| GET    | /recipes/:recipeTitle    | show all recipes matching recipeTitle                 |
| POST   | /recipes/add             | Add recipe                                            |
| POST   | /recipes/delete          | Delete recipe                                         |
//PRODUCTS LIST
| GET    | /productli/              | Display all productslist of logged in user            |
| GET    | /productsli/:recipe      | display products list associated with recipe          |
| POST   | /productli/add           | Add  productList                                      |
| POST   | /productli/delete        | delete  productList                                   |
//PRODUCTS
| POST   | /products/add            | add a product within a specified productList          |
| POST   | /products/delete         | delete product by product_name within a productList   |
| GET    | /products/:productlist   | Display all products within specified productlist     |
| GET    | /products/:product_name  | Display product by matching product_name              |


## DB STRUCTURE:

/*

MODELS STRUCTURE

USER:
    email: "string",
    password: "string",
    pantries: array,
    recipesList: array,

PANTRY: 
    pantryName: "string",
    ingredients: "ingredients,"
    date_created: "string"

RECIPESLIST:
    ids: ["333",""]
    
RECIPES:
    _id:"333 "
    recipeTitle: "string",
    image: "string",
    cooking_time: "string",
    nutrition labels: "strings",
    receipe_link: "string",
    productList: array, <-- entry for products
    category: [italian, "pasta", ]

PRODUCTSLIST:

    ids: ["444"]

PRODUCT:
    _id: "444"
    image:"string",
    product_Name: "string",
    lowest_price: number,
    upc: "string",
    sellers: array

    SELLER:
        seller: "string",
        productTitle: "string",
        price: number,
        shipping: price,
        link: "string"

*/
const DB = [{
    user: "userName",
    password: "encripted password",
    //SAVED PANTRY
    pantries: [
    {
      pantry_name: "Asian ingredients",
      dateCreated: '12/17/2020',
      //var today = new Date();
    // var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      userIngredients: ["apple", "banana", "milk", "etc..."],
    }],
    
    //SAVED RECIPES TO EXCLUDE PRODUCTS LIST
    recipesList: [{
        //mongoose populate
      //user will type in Category when they are prompted to save
      category: "italian",
  
      recipes: [{
        recipeTitle: "Apple Crumble",
        image: "image link",
        total_ingredients: [],
        cooking_time: "cooking time",
        nutrition_labels: "labels, labels",
        recipe_link: "original link",
        //PRODUCTS LIST will be saved in different page outside of saved recipes, but will reference back original recipe title
        productsList: [{
          listName: "listName",
          //PRODUCT 1
          products: [{
              product_Name: 'Hodgson Mill Rice Flour, 16 oz',
              lowest_price: 3.04,
              upc: "upc code",
              //SUB CATEGORY OF PRODUCTS LIST to be organized in table format
              sellers: [{
                  seller: "Amazon.com",
                  title: "Hodgson Mill Rice Flour, Case of 6",
                  price: 21.91,
                  shipping: 11.00,
                  Link: "original link",
                },
                {
                  seller: "Walmart.com",
                  title: "Hodgson Mill Rice Flour, Case of 6",
                  price: 21.91,
                  shipping: 11.00,
                  Link: "original link",
                },
              ]
            },
            //PRODUCT 2
            {
              product_Name: 'Gummy Vitamins, 90ct',
              lowest_price: 3.04,
              upc: "upc code",
              //SUB CATEGORY OF PRODUCTS LIST to be organized in table format
              sellers: [{
                  seller: "Amazon.com",
                  title: "Gummy Vitamins medium bottle",
                  price: 21.91,
                  shipping: 11.00,
                  Link: "original link",
                },
                {
                  seller: "Walmart.com",
                  title: "Gummy Vitamins medium bottle",
                  price: 21.91,
                  shipping: 11.00,
                  Link: "original link",
                },
              ]
            },
          ]
        }]
      }, ]
    }],
  }, ]

***Your solution goes to the current folder***