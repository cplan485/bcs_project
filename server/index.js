const app = require('express')();
// const express = require('express');
// const app = express();

//DEPLOY DIGITAL OCEAN AND FOR TESTING AT LOCALHOST
 const express = require('express');
 const path = require('path');

 
  //END DEPLOY
const port = process.env.port || 3040;

recipesRoute = require('./routes/recipes.routes'),
pantryRoute = require('./routes/pantry.routes'),
require('dotenv').config();
// =============== BODY PARSER SETTINGS =====================
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting() {
	try {
		//'mongodb://127.0.0.1/test_mongodb'
		await mongoose.connect(process.env.MONGO, { useUnifiedTopology: true, useNewUrlParser: true });
		console.log('Connected to the DB');
	} catch (error) {
		console.log('ERROR: Seems like your DB is not running, please start it up !!!');
	}
}
connecting();
//================ CORS ================================
const cors = require('cors');
app.use(cors());
// =============== ROUTES ==============================
app.use('/users', require('./routes/users.routes'));
//app.use('/recipes', recipesRoute);
app.use('/pantries', pantryRoute);

//MAILER 
//or enable it only for the specific url
app.options('/sendEmail', cors())

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

	next();
});
//=============================================================================================== 
//=============================================================================================== 
//=============================================================================================== 

app.use('/emails', require('./routes/emails.routes.js'))

//MAILER END

//STRIPE

require("dotenv").config({ path: "./.env" });

app.use("/payment", require("./routes/payment.route.js"));

//STRIPE END
app.use('/recipes', recipesRoute);

app.use(express.static(__dirname));
 app.use(express.static(path.join(__dirname, '../client/build')));

 app.get('/*', function (req, res) {
 	res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
   });
// =============== START SERVER =====================
app.listen(port, () => console.log(`server listening on port ${port}`));
