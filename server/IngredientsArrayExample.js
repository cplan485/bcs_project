const DB = [
    {
    user: "userName", 
    pantry: {
    dateCreated: '12/17/2020', 
    userIngredients: ["apple", "banana", "milk" ], 
            },
    recipes: {
        //user will type in Category
        category: "italian",

        recipes: [ 
            {recipeTitle: "Apple Crumble",
        image: "image link",
        total_ingredients: [],
        cooking_time: "cooking time",
        nutrition_labels: "labels, labels",
        recipe_link: "original link",

        productsList: [ 
            //PRODUCT 1
            { product_Name: 'Hodgson Mill Rice Flour, 16 oz',
        lowest_price: 3.04,
        upc: "upc code",
        sellers: [ {
            seller: "Amazon.com",
            title: "Hodgson Mill Rice Flour, Case of 6",
            price: 21.91,
            shipping: 11.00,
            Link: "original link",
            },
            {
            seller: "Walmart.com",
            title: "Hodgson Mill Rice Flour, Case of 6",
            price: 21.91,
            shipping: 11.00,
            Link: "original link",
            }, ]
        },
            //PRODUCT 2
        { product_Name: 'Gummy Vitamins, 90ct',
        lowest_price: 3.04,
        upc: "upc code",
        sellers: [ {
            seller: "Amazon.com",
            title: "Gummy Vitamins medium bottle",
            price: 21.91,
            shipping: 11.00,
            Link: "original link",
            },
            {
            seller: "Walmart.com",
            title: "Gummy Vitamins medium bottle",
            price: 21.91,
            shipping: 11.00,
            Link: "original link",
            }, ]
        },
                    ]    
            },
        ]
    },
},
]