import React from 'react';
import { MdAddCircle } from 'react-icons/md';
import {HiOutlineEmojiSad } from 'react-icons/hi'
import { TiShoppingCart } from 'react-icons/ti';
import foodicon2 from './images/foodicon2.svg';
import foodicon3 from './images/foodicon3.svg';
import { IconContext } from "react-icons";
import { MdStore } from "react-icons/md";
import {MdAddCircleOutline } from 'react-icons/md'
import { RiContactsBookLine, RiCreativeCommonsZeroLine, RiPlayListAddFill } from "react-icons/ri";
import { FaSleigh } from 'react-icons/fa';
import ReactTooltip from 'react-tooltip';
//import Checkbox from '@material-ui/core/Checkbox';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';


class IngredientsList extends React.Component{


  state = { 
    fetching: "",
    showIngredients: false,
    showMissingIngredients: false,
    showPotentialProducts: true,
    showSellersInfo: true,
    showSellerData: [],
    shoppingListIngredients:[],
    shoppingLiProducts: [],
    checkedBoxList: [],
    priceData: [],
    priceNoZeros: [],
    pricing: []
  }

  APICall =() => {
    let self=this;
    var request = require('request');
var options = {
  'method': 'POST',
  'url': 'https://api.spoonacular.com/food/ingredients/map?apiKey=76d2babab5a542018ed614b5e1aa89bd',
  'headers': {
    'Content-Type': 'text/plain',
    'Cookie': '__cfduid=d81d8f29a3c608530065734df139e6dab1607700494'
  },
  body: `{ "ingredients": ["${self.state.shoppingListIngredients}"] }`

};
request(options, function (error, response) {
  if (error) throw new Error(error);
  let products = [];
  let meta = [];
  let res = response.body;
  //CONVERT JSON TO A JAVASCRIPT OBJECT WITH PARSE
 let responseObj = JSON.parse(res)
 responseObj.forEach(obj =>{ products.push(obj.products)
  meta.push(obj.meta[0])
});
 
  self.setState({shoppingLiProducts: products, metaArr: meta});
  self.setState({metaArr: meta});
  
});

  }

  APICallBarCode =() => {
    this.setState({fetching: "fetching..."})
    let self = this;
    let products = self.state.checkedBoxList.filter(Boolean);
    var myHeaders = new Headers();
    myHeaders.append("x-rapidapi-host", "product-data1.p.rapidapi.com");
    myHeaders.append("x-rapidapi-key", "2f5f8d0f0amshe1c70c240b8ac4ap14f9a7jsn0c893f6b725d");
    
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

products.forEach(obj =>{
fetch(`https://product-data1.p.rapidapi.com/lookup?upc=${obj}`, requestOptions)
  .then(response => response.text())
  .then(result =>{ //console.log(result)
    
    let parsed = JSON.parse(result);
    let imageVar;
    parsed.items.images[0]? imageVar =parsed.items.images[0] : imageVar = foodicon2;
    let resultObj = {image: imageVar, 
      title:parsed.items.title, 
      upc: parsed.items.upc,
      lowestprice: parsed.items.lowest_pricing
    };
    self.state.priceData.push(resultObj);
    self.state.pricing.push(parsed.items.pricing);
    //self.setState({pricing: parsed.items.pricing})
    self.setState({fetching: ""})
    self.setState({checkedBoxList: []})
    console.log(`PriceDATA`, self.state.priceData)
    console.log(`Pricing`, self.state.pricing)
  }
  )
  
  .catch(error => console.log('error', error));
  }
)
  }

  addDefaultSrc =(ev) =>{
    ev.target.src = foodicon3;
  }


 
    render(){
      
      //console.log(`Checking My Ingredients PROPS`, this.props)
      //console.log(`checking Shopping LI list`, this.state.shoppingListIngredients)
      
     let self= this;
      let ingredientsArray=[];
      
      this.props.products.forEach(obj =>{
        ingredientsArray.push(obj)
      })

      let removedDup = ingredientsArray.filter((v,i,a)=>a.findIndex(t=>(t.text === v.text))===i)
      let missedIngredients = [...removedDup];
   
      this.props.ingredients.forEach(obj => {
        missedIngredients.forEach((item,idx) =>{
          
          if (item.text.includes(obj)) {
            missedIngredients.splice(idx, 1)
          }
        })
        // missedIngredients.push( ingredientsArray.filter(user => user.text.includes(obj)== false) )
       })
      
       let shoppingListIngredients = [];
       missedIngredients.forEach(obj=> {
        shoppingListIngredients.push(obj.text);
       })

       let renderShoppingList =() => {
          let stringed = shoppingListIngredients.join(`","`)
           self.setState({shoppingListIngredients:stringed})
           console.log(stringed)
       }
       


  //console.log(`remove dup`, removedDup)
    let renderIngredients =(arr, add) => (
        arr.map( (item, idx) => {
         // console.log(`original array`, item.text)
          return <p key={idx}><img className="ingredientsImages" src={item.image ? item.image: foodicon2}></img>{item.text}
          {add === true ? <MdAddCircleOutline className="icons" onClick={() =>
         this.props.submitInput(item.text)
        } data-tip="Add to pantry"/> : null }
        
        </p>
        })
      )

      let renderPotentialProducts =(arr) => (
        arr.map( (item, idx) => {
          console.log(`testing new potentail items`, this.state.metaArr)
         if (item !== undefined ) {
          
         return <div >
           <h1>Product {idx + 1} : {this.state.metaArr[idx]}</h1>
           <div className="products_sublist">
          {renderSuggested_SubList(arr[idx]) }
          </div>
         </div> 
         }
         else {
           return <p> {idx + 1}) No Product Found</p>
         }
        })
      )


      let renderSuggested_SubList =(arr) => (
       
        arr.map( (item, idx) => {
          
         // console.log(`original array`, item.text)
         if (item !== undefined && idx <4 ) {
          return <div class="productListing" key={idx}>
            <label>
            <input type="checkbox" name="selected_product" value={item.upc} onChange={handleInputChange} />
            
            <h2>
              {idx+1})
            </h2>  {item.title} </label>
            
            
          </div>
          
         }
         else if (idx >=4) {
           return null
         }
         else {
           return <p> {idx + 1}) No Product Found</p>
         }
        })
      )

      
      let handleInputChange = (event) => {
        const target = event.target;
        var value = target.value;
        
        if(target.checked){
            this.state.checkedBoxList.push(value);
            console.log(`checking Check boxes,`, this.state.checkedBoxList);   
        }else{
            this.state.checkedBoxList.splice(value, 1);
        }
        
    }

         let renderPrices =(arr) => (
           
        arr.map( (item, idx) => {
          let tempArr = [...this.state.showSellerData];
          let tempPriceArr = [...this.state.pricing];
          let lowestPriceArr =[];
          tempArr.push(false);
          let lowest;
          console.log(`Temporary PRICE Arr`, tempPriceArr);
          //console.log(`price 0`, tempPriceArr[0][0].price);
          let noZeros = tempPriceArr[idx].filter(user => user.price !== 0);
          
          
          if (noZeros.length >1) {
          lowest = noZeros.reduce((min, p) => p.price < min ? p.price : min, noZeros[0].price);
          }
          else if (noZeros.length ==1) {
            lowest = noZeros[0].price;
          }
          else {
            lowest=0;
          }
          lowestPriceArr.push(lowest);
          
          this.state.priceNoZeros.push(lowest);

          if (item !== undefined  && lowest !==0) {
          return <div className="pricesContainer" key={idx}>
            <h2>{this.state.metaArr[idx]}</h2>
            <img className="pricedImages" src={item.image !== null || item.image !== undefined ? item.image : foodicon2} alt="Image not found" onError={self.addDefaultSrc}></img> {idx+1}) {item.title}  <h2>
            Lowest Price:
          </h2> $ {lowest.toFixed(2)} 
        
          {/* <h2>upc: {item.upc}</h2> */}
          <IconContext.Provider value={{className: "icons", size: "2em" }}>
         <MdStore data-tip="Display sellers" className="icons" onClick={() => {
          tempArr[idx] = !tempArr[idx];
          this.setState({showSellerData:tempArr})
          }} />
         {/* <ReactTooltip place="right"/> */}
         </IconContext.Provider> ( {this.state.pricing[idx].length} ) Sellers
       { this.state.showSellerData[idx]? <div className="stores"> {renderStores(this.state.pricing[idx])}</div> : null }
        {/* {console.log(`my seller data`, this.state.idx)}  */}
          </div>
          }
          else {
            return <div className="pricesContainer"><h2>{this.state.metaArr[idx]}</h2>
              <img className="pricedImages" src={item.image !== null || item.image !== undefined ? item.image : foodicon3} alt="Image not found" onError={self.addDefaultSrc}></img>
              <p> {idx + 1}) <IconContext.Provider value={{className: "icons", size: "2em" }}><HiOutlineEmojiSad /> </IconContext.Provider> No product or valid prices Found</p> 
              </div>
          }
        })
      )

      let renderStores =(arr) => (
        
        arr.map( (item, idx) => {
          
         
          return <div key={idx} className="renderSeller"> {idx+1}) 
          <h2>
            {item.seller}
          </h2> 
           <h2>
            {item.title}
          </h2>

          <div className ="mainRows"><h2> Price: </h2> $ {parseFloat(item.price).toFixed(2) } ,
          {/* <h2>Shipping:</h2> $ {parseFloat(item.shipping).toFixed(2)} */}
          </div>
            
          <h2>
          <a href={item.link} target="_blank">Link</a>
          </h2>
           </div>
        })

      
      )

      let reduceDupLowPrices =() => {
        let reduced = this.state.priceNoZeros.filter((v,i,a)=>a.findIndex(t=>(t === v))===i);
        //console.log(`REDUCED DUP`, reduced);
        this.setState({priceNoZeros: reduced})

    }
      
        return (
          
        this.props.products.length>0 ?<div>
          <IconContext.Provider value={{className: "icons", size: "1.5em" }}>
          <MdAddCircle className="icons" onClick={() =>this.setState({showIngredients:!this.state.showIngredients})} /> 
          </IconContext.Provider>
          
        {this.state.showIngredients == true ? <div className="products">{renderIngredients(removedDup)}
        </div> : null }

        
        <h2> {missedIngredients.length} Missing Ingredients</h2> 
          <IconContext.Provider value={{className: "icons", size: "1.5em" }}>
        <MdAddCircle className="icons" onClick={() =>this.setState({showMissingIngredients:!this.state.showMissingIngredients})} />
        
        </IconContext.Provider>
        
        {this.state.showMissingIngredients == true ? <div> {renderIngredients(missedIngredients, true)} 
        <IconContext.Provider value={{className: "icons", size: "1.5em" }}>
        <TiShoppingCart data-tip="Find Suggested Products" onMouseEnter={renderShoppingList} onClick={this.APICall} className="icons" />
       <ReactTooltip />
        </IconContext.Provider>
        </div>: null }
        {/* THIS COULD BE A SEPARATE COMPONENT! */}

       { this.state.shoppingLiProducts.length>0 ? <div className="pricingContainer"><h2>
         Suggested Products</h2>
         <IconContext.Provider value={{className: "icons", size: "1.5em" }}>
         <MdAddCircle className="icons" onClick={() =>this.setState({showPotentialProducts:!this.state.showPotentialProducts})} />
         </IconContext.Provider>
         {this.state.showPotentialProducts? renderPotentialProducts(this.state.shoppingLiProducts)  : null}
         
         {this.state.priceData.length>0 ? null :
         <Button onClick={this.APICallBarCode} size="small" variant="contained" color="primary" style={{backgroundColor: "#F20530"}}>    
         Check Prices
       </Button>
          }
         </div> 
         : null }
         {this.state.fetching}
          { this.state.priceData.length>0? <div className="finalPrices"><h2>
         Price Info</h2>
         <IconContext.Provider value={{className: "icons", size: "1.5em" }}>
         <MdAddCircle className="icons" onClick={() =>this.setState({showSellersInfo:!this.state.showSellersInfo})} />
         <RiPlayListAddFill data-tip="Save to Products List" className="icons" onClick="" />
         <ReactTooltip />
         </IconContext.Provider>
         {this.state.showSellersInfo ? <div>
         {renderPrices(this.state.priceData) }
         <h2>
           Total Shopping List Estimated Price:
         </h2> 

         {console.log(`noZeros From RENDER`, this.state.priceNoZeros.filter((v,i,a)=>a.findIndex(t=>(t === v))===i)
         )}
         {reduceDupLowPrices}
         $ {Math.round(this.state.priceNoZeros.filter((v,i,a)=>a.findIndex(t=>(t === v))===i)
         .reduce((sum, user) => sum + user, 0) *100)/100} </div> : null }
         </div> : null } 

        </div>
         : null
           
        )
    }

}

export default IngredientsList