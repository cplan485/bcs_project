import React from 'react';
import SingleRecipe from './SingleRecipe';
import { URL } from '../url';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import Icon from '@material-ui/core/Icon';

class SavedPantries extends React.Component{

  state = {
    pantries: [],
    email: this.props.email,
    name: this.props.name,
  }
  

    getPantries = async () => {
        
        try {
         
         let stringed = {
            email:`carlos_planchart@live.com`,
            };

          const response = await axios.get(`${URL}/pantries/${this.state.email}`, {params: stringed
          }
            );
          //setMessage(response.data.message);
         // console.log(`Data from Saved Pantries`,response)
         
          if (response.status ==200) {
           // console.log(`ginger`, response.data.pantries)
            this.setState({pantries: response.data.pantries})
            // setTimeout(() => {
            //   this.props.history.push('/login');
            // }, 2000);
          }
        } catch (error) {
          console.log(error);
        }
      };


      componentDidMount() {
        this.getPantries();
      }

      deletePantry = async (name,index) => {
        
        try {
         
          const response = await axios.post(`${URL}/pantries/delete`, {
            email: this.props.email,
            pantryName: name
          });
        
         // console.log(`Data from Saved Pantries`,response)
         
          if (response.status ==200) {
           
            let pantryDup =[...this.state.pantries]
            pantryDup.splice(index,1);
            
            this.setState({pantries: pantryDup})
            // setTimeout(() => {
            //   this.props.history.push('/login');
            // }, 2000);
          }
        } catch (error) {
          console.log(error);
        }
      };

      componentDidMount() {
        this.getPantries();
      }

      renderPantryIngredients =(arr) => (
        
        arr.map( (item, idx) => {
          
         
          return <div key={idx} className="pantryIngredients">
          <p>
          {idx+1}) {item}
          </p>
           </div>
        })
      )

      renderPantries =(arr) => (
        
        arr.map( (item, idx) => {
          
         
          return <div key={idx} className="savedPantries"> 
          <h2>
          {idx+1}) {item.pantryName}
          </h2> 
           <h3>
            Created: {item.dateCreated}
          </h3>
          <div className="savedPantryList">
          {this.renderPantryIngredients(item.ingredients)} </div>
          <Button
        variant="contained"
        onClick={() => this.deletePantry(item.pantryName, idx)
       }
        color="secondary"
        //className={classes.button}
        startIcon={<DeleteIcon />}
      >
        Delete
      </Button>
           </div>
        })
      )

  


    render(){ 
  //console.log(`props from Saved Pantries`,this.props)
 // console.log(`props from Saved Pantries`,this.state.pantries)
   
        return (
          <div className="MainSavedPantries">
            <h1 className="titles">Welcome back {this.state.name}</h1>
            <h2 className="subtitles"> Your Pantries:</h2>
        <div className="myPantries">
         {this.state.pantries.length > 0 ? this.renderPantries(this.state.pantries): <div>
           You have not saved any patries yet!
         </div> }
        </div> 
        </div>
        )
    }

}

export default SavedPantries