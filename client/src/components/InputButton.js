import React from 'react';

const InputButton = (props) => {
    return <div>
    <button onClick={props.submit}>{props.buttonText}</button>

    </div>
}

export default InputButton