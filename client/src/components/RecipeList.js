import React from 'react';
import SingleRecipe from './SingleRecipe';

class RecipeList extends React.Component{
    render(){ 
    
    let renderProducts =(arr) => (
        arr.map( (item, idx) => {
       
          return <SingleRecipe product={item} key={idx}   
          ingredients={this.props.ingredients} submitInput={this.props.submitInput} loggedIn={this.props.loggedIn} email={this.props.email}
          />
        })
      )
        return (
        <div className="products">{renderProducts(this.props.products)}</div> 
        )
    }

}

export default RecipeList