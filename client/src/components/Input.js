import React from 'react';

const Input = (props) => {
    return <div>
    <input onChange={props.inputChange} name='lname' value={props.input} placeholder="type in an ingredient here..."/>
    </div>
}

export default Input