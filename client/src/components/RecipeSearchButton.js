import React from 'react';

const RecipeSearchButton = (props) => {
    return <div>
    <button onClick={props.submit}>Search Recipe!</button>

    </div>
}

export default RecipeSearchButton