import React, {useState} from 'react';
import { FaRegTrashAlt } from 'react-icons/fa';
import {FaPen} from 'react-icons/fa';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

function Ingredient({ todo, index, removeTodo, handleTodo, submitPantryChange }) {
  const [isShown, setIsShown] = useState(false);
  const [changeTodo, setChangeTodo] = useState(false);

  const [ form, setValues ] = useState({
		todo: '',
  });


  

  
    return (
      <div
        className="ingredient-row"
        onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}
      >
        {index + 1} : {todo} 
        
          {isShown && (
            <FaRegTrashAlt onClick={() => removeTodo(index)} className="icons"/>
            
            
            
          )}
          {isShown && (
        <FaPen onClick={() => setChangeTodo(!changeTodo)} className="icons"/>
          )}
          {changeTodo ?  <form /* onSubmit={handleSubmit}*/onChange={handleTodo} className="pantryChange_form">
          <TextField
          //id="standard-full-width"
          size="small"
          name="todo"
          onChange={handleTodo}
          style={{ margin: 0, padding: 0, }}
          placeholder={todo}
          style={{color: "white", backgroundColor: "rgb(238 255 145)", marginRight: "1em",marginTop:".5em",}}
          fullWidth
         // value={todo}
          margin="normal"
           variant="filled"
          InputLabelProps={{
            shrink: true,
          }}
        />
         <Button onClick={() => { submitPantryChange(index); setChangeTodo(!changeTodo)}} size="small" style={{color: "white", backgroundColor: "#F20530", marginRight: "1em"}}  color="primary">Change</Button> 
         <Button onClick={() => setChangeTodo(!changeTodo)} size="small" style={{color: "white", backgroundColor: "#F23D6D"}}  color="primary">X</Button>
         </form>  : null}
      </div>
    );
  }
export default Ingredient;