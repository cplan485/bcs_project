import React from "react";
import Checkout from "../containers/checkout";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = props => {
  console.log('API Key from stripe',process.env.REACT_APP_STRIPE_PUBLIC_KEY)
  return (
    <StripeProvider apiKey={process.env.REACT_APP_STRIPE_PUBLIC_KEY}>
      <Elements>
        <Checkout {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
