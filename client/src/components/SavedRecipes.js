import React from 'react';
import { URL } from '../url';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import Icon from '@material-ui/core/Icon';
import foodicon from './images/foodPlate.svg';
import { IconContext } from "react-icons";
import { MdAddCircle } from 'react-icons/md';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';

class SavedPantries extends React.Component{

  state = {
    recipes: [],
    email: this.props.email,
    name: this.props.name,
    showIngredients: [],
    showAdditionalNutrition: [],
  }
  

    getRecipes = async () => {
        
        try {
         
       

          const response = await axios.get(`${URL}/recipes/${this.state.email}`, {params: {email:  this.state.email}
          }
          
            );
            
        
          //setMessage(response.data.message);
          console.log(`Data from Saved Recipes`,response)
         
          if (response.status ==200) {
            console.log(`ginger`, response.data.recipes)
            this.setState({recipes: response.data.recipes})
            // setTimeout(() => {
            //   this.props.history.push('/login');
            // }, 2000);
          }
        } catch (error) {
          console.log(error);
        }
      };

      addDefaultSrc =(ev) =>{
        ev.target.src = foodicon;
      }


      componentDidMount() {
        this.getRecipes();
      }

      deletePantry = async (name,index) => {
        
        try {
         
          const response = await axios.post(`${URL}/recipes/delete`, {
            email: this.props.email,
            recipeTitle: name
          });
          
        
          console.log(`Data from Saved Recipes Delete`,response)
         
          if (response.status ==200) {
           
            let recipeDup =[...this.state.recipes]
            recipeDup.splice(index,1);
            
            this.setState({recipes: recipeDup})
            // setTimeout(() => {
            //   this.props.history.push('/login');
            // }, 2000);
          }
        } catch (error) {
          console.log(error);
        }
      };

      componentDidMount() {
        this.getRecipes();
      }

      renderRecipeIngredients =(arr) => (
        
        arr.map( (item, idx) => {
          
         
          return <div key={idx}>
          <p>
          <img className="ingredientsImages" src={item.image? item.image: foodicon } onError={this.addDefaultSrc} ></img>
          {idx+1}) {item.text}
          
          </p>
           </div>
        })
      )

      renderNutritionInfo =(arr) => (
        
        arr.map( (item, idx) => {
          
         
          return <div key={idx}>
          <p>
          
          {item.label}: {item.total.toFixed(2)}
          
          </p>
           </div>
        })
      )

      renderRecipes =(arr) => (
        
        arr.map( (item, idx) => {
         let dupArr = [...this.state.showIngredients];
         dupArr.push(false);

         let dupNutritionArr = [...this.state.showAdditionalNutrition];
         dupNutritionArr.push(false);
          return <div key={idx} className="singleRecipe"> 
          <h2>
          {idx+1}) {item.recipeTitle}
          </h2> 
          <h3> Category: {item.category}</h3>
          <img src={item.image? item.image: foodicon } onError={this.addDefaultSrc} ></img>
           <h3>
            Created: {item.dateCreated}
          </h3>
          
          <div className="savedPantryList">
          <h2>{item.ingredients.length} Ingredients</h2>
          <IconContext.Provider value={{className: "icon", size: "1.5em" }}>              
         <MdAddCircle className="icons" onClick={() =>{
           dupArr[idx] =! dupArr[idx];
           this.setState({showIngredients:dupArr})
         }
        }
         />
            </IconContext.Provider>
            {this.state.showIngredients[idx] == true ? this.renderRecipeIngredients(item.ingredients): null}
           </div>
          <h2>Total Cooking Time: {item.cookingTime} Minutes </h2>
          <p>{item.nutritionLabels}</p>
          <h2>Full Nutritional Values:</h2>

          <IconContext.Provider value={{className: "icon", size: "1.5em" }}>              
         <MdAddCircle className="icons" onClick={() =>{
           dupNutritionArr[idx] =! dupNutritionArr[idx];
           this.setState({showAdditionalNutrition:dupNutritionArr})
         }
        }
         />
            </IconContext.Provider>
            {this.state.showAdditionalNutrition[idx] == true ? this.renderNutritionInfo(item.digest) : null }
          <div><a href={item.recipeLink}>{item.recipeSource} Original Link</a></div>
          <h2>Your Comments</h2>
          <TextField
          id="outlined-multiline-static"
          label="Type Comments here!"
          multiline
          rows={5}
          placeholder="I love this recipe!, but it could use some..."
          variant="filled"
        

          style={{backgroundColor: "#FBF1CF", width: "100%"  }}
        />

<Button
        variant="contained"
        color="primary"
        style={{backgroundColor: "#B9BF04", marginTop: "1em"  }} 
        startIcon={<SaveIcon />}
      >
        Save Comment
      </Button>
          <Button
        variant="contained"
        style={{backgroundColor: "#F20530", marginTop: "1em"  }}
        onClick={() => this.deletePantry(item.recipeTitle, idx)
       }
        color="secondary"
        //className={classes.button}
        startIcon={<DeleteIcon />}
      >
        Delete Recipe
      </Button>
           </div>
        })
      )

  


    render(){ 
  console.log(`props from Saved Recipes`,this.props)
  console.log(`props from Saved Recipes - Recipes`,this.state.recipes)
  console.log(`props from Saved Recipes - Show Ingredients`,this.state.showIngredients)
   
        return (
          <div >
            <h1 className="titles">Welcome back {this.state.name}</h1>
            <h2 className="subtitles"> Your Recipes:</h2>
        <div className="mainSavedRecipes">
            
          {this.state.recipes.length > 0 ? this.renderRecipes(this.state.recipes): <div>
           You have not saved any patries yet!
         </div> } 

        </div> 
        </div>
        )
    }

}

export default SavedPantries