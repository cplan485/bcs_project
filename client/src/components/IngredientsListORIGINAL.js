import React from 'react';
import { MdAddCircle } from 'react-icons/md';
import { FiList } from 'react-icons/fi';
import foodicon2 from './images/foodicon2.svg';
import { IconContext } from "react-icons";
import { MdStore } from "react-icons/md";
import { RiPlayListAddFill } from "react-icons/ri";
import { FaSleigh } from 'react-icons/fa';
import ReactTooltip from 'react-tooltip';


class IngredientsList extends React.Component{


  state = { 
    fetching: "",
    showIngredients: false,
    showMissingIngredients: false,
    showPotentialProducts: true,
    showSellersInfo: true,
    showSellerData: [],
    shoppingListIngredients:[],
    shoppingLiProducts: [],
    priceData: [],
    pricing: []
  }

  APICall =() => {
    let self=this;
    var request = require('request');
var options = {
  'method': 'POST',
  'url': 'https://api.spoonacular.com/food/ingredients/map?apiKey=76d2babab5a542018ed614b5e1aa89bd',
  'headers': {
    'Content-Type': 'text/plain',
    'Cookie': '__cfduid=d81d8f29a3c608530065734df139e6dab1607700494'
  },
  body: `{ "ingredients": ["${self.state.shoppingListIngredients}"] }`

};
request(options, function (error, response) {
  if (error) throw new Error(error);
  let products = [];
  let res = response.body;
  //CONVERT JSON TO A JAVASCRIPT OBJECT WITH PARSE
 let responseObj = JSON.parse(res)
 responseObj.forEach(obj =>{ products.push(obj.products[0])});
  self.setState({shoppingLiProducts: products});
  
});

  }

  APICallBarCode =() => {
    this.setState({fetching: "fetching..."})
    let self = this;
    let products = self.state.shoppingLiProducts.filter(Boolean);
    var myHeaders = new Headers();
    myHeaders.append("x-rapidapi-host", "product-data1.p.rapidapi.com");
    myHeaders.append("x-rapidapi-key", "2f5f8d0f0amshe1c70c240b8ac4ap14f9a7jsn0c893f6b725d");
    
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

products.forEach(obj =>{
fetch(`https://product-data1.p.rapidapi.com/lookup?upc=${obj.upc}`, requestOptions)
  .then(response => response.text())
  .then(result =>{ console.log(result)
    
    let parsed = JSON.parse(result);
    let resultObj = {image: parsed.items.images[0], 
      title:parsed.items.title, 
      upc: parsed.items.upc,
      lowestprice: parsed.items.lowest_pricing
    };
    self.state.priceData.push(resultObj);
    self.state.pricing.push(parsed.items.pricing);
    //self.setState({pricing: parsed.items.pricing})
    self.setState({fetching: ""})
    console.log(`PriceDATA`, self.state.priceData)
    console.log(`Pricing`, self.state.pricing)
  }
  )
  
  .catch(error => console.log('error', error));
  }
)
  }


 
    render(){

      console.log(`SHOPPING LIST NOT WORKING`, this.state.shoppingLiProducts)

     let self= this;
      let ingredientsArray=[];
      
      this.props.products.forEach(obj =>{
        ingredientsArray.push(obj)
      })

      let removedDup = ingredientsArray.filter((v,i,a)=>a.findIndex(t=>(t.text === v.text))===i)
      let missedIngredients = [...removedDup];
   
      this.props.ingredients.forEach(obj => {
        missedIngredients.forEach((item,idx) =>{
          
          if (item.text.includes(obj)) {
            missedIngredients.splice(idx, 1)
          }
        })
        // missedIngredients.push( ingredientsArray.filter(user => user.text.includes(obj)== false) )
       })
      
       let shoppingListIngredients = [];
       missedIngredients.forEach(obj=> {
        shoppingListIngredients.push(obj.text);
       })

       let renderShoppingList =() => {
          let stringed = shoppingListIngredients.join(`","`)
           self.setState({shoppingListIngredients:stringed})
           console.log(stringed)
       }
       


  //console.log(`remove dup`, removedDup)
    let renderIngredients =(arr) => (
        arr.map( (item, idx) => {
         // console.log(`original array`, item.text)
          return <p key={idx}><img className="ingredientsImages" src={item.image ? item.image: foodicon2}></img>{item.text}</p>
        })
      )

      let renderPotentialProducts =(arr) => (
        arr.map( (item, idx) => {
         // console.log(`original array`, item.text)
         if (item !== undefined ) {
          return <div key={idx}>
            <h2>
            {idx+1})  {item.title}
          </h2> 
          {/* <h2>
            UPC:
          </h2> {item.upc} */}
          </div>
         }
         else {
           return <p> {idx + 1}) No Product Found</p>
         }
        })
      )

      let renderPrices =(arr) => (
        arr.map( (item, idx) => {
          this.state.showSellerData.push(false);
          return <div className="pricesContainer" key={idx}><img className="pricedImages" src={item.image !== null || item.image !== undefined ? item.image : foodicon2} alt="Image not found" onerror="this.onerror=null;this.src='https://placeimg.com/200/300/animals';"></img> {idx+1}) {item.title}  <h2>
            Lowest Price:
          </h2> $ {parseFloat(item.lowestprice).toFixed(2)} 
          {/* <h2>upc: {item.upc}</h2> */}
          <IconContext.Provider value={{className: "icons", size: "2em" }}>
         <MdStore data-tip="Display sellers" className="icons" onClick={() =>this.state.showSellerData[idx] = !this.state.showSellerData[idx]} />
         {/* <ReactTooltip place="right"/> */}
         </IconContext.Provider> ( {this.state.pricing[idx].length} )
       { this.state.showSellerData[idx]? renderStores(this.state.pricing[idx]) : null }
          </div>
          
        })
      )

      let renderStores =(arr) => (
        arr.map( (item, idx) => {
         // console.log(`original array`, item.text)
          return <div key={idx} className="renderSeller"> {idx+1}) 
          <h2>
            {item.seller}
          </h2> 
           <h2>
            {item.title}
          </h2>

          <div className ="mainRows"><h2> Price: </h2> $ {parseFloat(item.price).toFixed(2) } ,
          <h2>Shipping:</h2> $ {parseFloat(item.shipping).toFixed(2)}</div>
            
          <h2>
          <a href={item.link} target="_blank">Link</a>
          </h2>
           </div>
        })

      
      )

        //down the pipeline if you successfully generate a products list then the APIBarcCode Call will be set in place.
      // if ( this.state.shoppingLiProducts.length>0) {this.APICallBarCode()
      // }
      
        return (
        this.props.products.length>0 ?<div>
          <IconContext.Provider value={{className: "icons", size: "2em" }}>
          <MdAddCircle className="icons" onClick={() =>this.setState({showIngredients:!this.state.showIngredients})} /> 
          </IconContext.Provider>
          
        {this.state.showIngredients == true ? <div className="products">{renderIngredients(removedDup)}
        </div> : null }

        
        <h2> {missedIngredients.length} Missing Ingredients</h2> 
          <IconContext.Provider value={{className: "icons", size: "2em" }}>
        <MdAddCircle className="icons" onClick={() =>this.setState({showMissingIngredients:!this.state.showMissingIngredients})} />
        <FiList data-tip="Find Suggested Products" onMouseEnter={renderShoppingList} onClick={this.APICall} className="icons" />
        <ReactTooltip place="bottom"/>
        </IconContext.Provider>
        
        {this.state.showMissingIngredients == true ? renderIngredients(missedIngredients) : null }
        {/* THIS COULD BE A SEPARATE COMPONENT! */}

       { this.state.shoppingLiProducts.length>0 ? <div className="pricingContainer"><h2>
         Suggested Products</h2>
         <IconContext.Provider value={{className: "icons", size: "2em" }}>
         <MdAddCircle className="icons" onClick={() =>this.setState({showPotentialProducts:!this.state.showPotentialProducts})} />
         {/* <RiPlayListAddFill className="icons" onClick="" /> */}
         </IconContext.Provider>
         {this.state.showPotentialProducts? renderPotentialProducts(this.state.shoppingLiProducts)  : null}
         
         {this.state.priceData.length>0 ? null :<button onClick={this.APICallBarCode}> Check Prices</button> }
         </div> 
         : null }
         {this.state.fetching}
          { this.state.priceData.length>0? <div className="finalPrices"><h2>
         Price Info</h2>
         <IconContext.Provider value={{className: "icons", size: "2em" }}>
         <MdAddCircle className="icons" onClick={() =>this.setState({showSellersInfo:!this.state.showSellersInfo})} />
         <RiPlayListAddFill data-tip="Save to Products List" className="icons" onClick="" />
         <ReactTooltip />
         </IconContext.Provider>
         {this.state.showSellersInfo ? <div>
         {renderPrices(this.state.priceData) }
         <h2>
           Total Shopping List Estimated Price:
         </h2> 
         $ {Math.round(this.state.priceData.reduce((sum, user) => sum + user.lowestprice, 0) *100)/100} </div> : null }
         </div> : null } 

        </div>
         : null
           
        )
    }

}

export default IngredientsList