import React from 'react'
import { SiAddthis } from 'react-icons/si';
import { IconContext } from "react-icons";
import IngredientsList from './IngredientsList';
import { MdAddCircle } from 'react-icons/md';
import { withRouter} from 'react-router-dom';
import foodicon from './images/foodPlate.svg';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { URL } from '../url';

class SingleRecipe extends React.Component {
    state = { showAdd: false,
        showAddRecipe: false,
        showRecipeInfo: true,
        saveRecipe: false,
        saveRecipeInput: '',
        alert: false,
    }

    addDefaultSrc =(ev) =>{
        ev.target.src = foodicon;
      }

      handleInput = (e) => {
        this.setState({saveRecipeInput: e.target.value});
      }

      toggleAlert = () => {
        this.setState({alert: !this.state.alert});
      }

      saveRecipe = async (e) => {
        this.toggleAlert();
        e.preventDefault();
        try {
      
          var today = new Date();
          var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
          const response = await axios.post(`${URL}/recipes/add`, {
            recipeTitle: this.props.product.recipe.label,
             category: this.state.saveRecipeInput,
             email: this.props.email[0],
             image: this.props.product.recipe.image,
             ingredients: this.props.product.recipe.ingredients,
             cookingTime: this.props.product.recipe.totalTime, 
             recipeSource: this.props.product.recipe.source,
             recipeLink: this.props.product.recipe.url,
             nutritionLabels: this.props.product.recipe.healthLabels.join(','),
             totalTime: this.props.product.recipe.totalTime,
             digest: this.props.product.recipe.digest,
            dateCreated: date
          });
          if (response.data.ok) {
            // this.setState({pantrySubmitSuccess: true})
           // console.log(this.state.ingredients)
            setTimeout(() => {
              this.props.history.push('/recipes');
            }, 2000);
          }
        } catch (error) {
          console.log(error);
        }
      };
       
    render() {
        let self = this;
            

        let {product, ingredients} = this.props
        console.log(`props from singleRecipe`, this.props)
        console.log(`state from singleRecipe`, this.state.alert)
       let healthLabelstring = product.recipe.healthLabels.join(',');
     
    return (<div className="singleRecipe" >
        <div class="container">
        {/* src={item.image ? item.image: foodicon2 */}
            <img src={product.recipe.image? product.recipe.image: foodicon } onError={self.addDefaultSrc} ></img>
            <div class="overlay" onMouseEnter ={() =>self.setState({showAdd:true, showAddRecipe:true})} onMouseLeave={() => self.setState({showAdd:false})}>
            {this.state.showAdd == true && (
            <div class="iconDiv">
            <IconContext.Provider value={{className: "icon", size: "2em" }}>              
            <SiAddthis onClick={() => self.setState({saveRecipe: !self.state.saveRecipe})}/>
            
            {this.state.showAddRecipe == true && this.state.saveRecipe === false ? <h2>Add to Favorite Recipes!</h2> : null}
            </IconContext.Provider>
            
            </div>
          )}
            </div>
           
        </div>
        {self.state.saveRecipe? 
               <div className="saveRecipe">
                   <TextField
               id="standard-full-width"
               label="Category?"
               size="small"
               onChange={this.handleInput}
               style={{ margin: 0 }}
               placeholder="Italian,Thai..etc"
              // helperText="save to your personal."
               fullWidth
               value={this.state.input}
               margin="normal"
               variant="outlined"
               InputLabelProps={{
               shrink: true,
               }}
               />
               <Button onClick={self.props.loggedIn ? self.saveRecipe: self.toggleAlert} style={{ height: 30, margin: 5,  }} variant="contained" color="secondary">Save!</Button>
               <Button onClick={() => self.setState({saveRecipe: !self.state.saveRecipe})} style={{ height: 30, margin: 5, backgroundColor: "#B9BF04" }} variant="contained" color="primary">X</Button>
               {self.state.alert && self.props.loggedIn? <div>Recipe saved!</div>: self.state.alert? <div className="error">Please login</div> : null}
            
               </div> : null
        
        }
       
         <h1 >{product.recipe.label}</h1>
         <h2>{product.recipe.ingredients.length} Total Ingredients </h2>
          <IngredientsList products={product.recipe.ingredients} ingredients={ingredients} submitInput={this.props.submitInput}/> 
         
         <h2>Total Cooking Time: {product.recipe.totalTime} minutes</h2>
         <IconContext.Provider value={{className: "icon", size: "1.5em" }}>              
         <MdAddCircle className="icons" onClick={() =>this.setState({showRecipeInfo:!this.state.showRecipeInfo})}
         
         />
            </IconContext.Provider>
        {this.state.showRecipeInfo ? <div>
         <p>{healthLabelstring}</p>
         <h3>Nutrition</h3>
         <div>Total Fat: {Math.round(product.recipe.digest[0].total *10)/10},
            Total Carbs: {Math.round(product.recipe.digest[1].total * 10)/10}, 
            Total Protein: {Math.round(product.recipe.digest[2].total * 10)/10}
         </div>
         <a href={product.recipe.url} target="_blank" > {product.recipe.source} Original Link</a></div> : null
        }
        {/* <p onClick={()=> this.props.history.push(product.recipe.url)}>Test Link</p> */}
         </div>
    )

    }
}


export default withRouter(SingleRecipe)