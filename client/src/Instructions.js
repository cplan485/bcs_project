import React from 'react'
import foodicon2 from './components/images/foodicon2.svg';
import userPass from './components/images/userPass.svg';
import SearchRecipes from './components/images/SearchRecipes.svg';
import savedPantriesAndRecipes from './components/images/savedPantriesAndRecipes.svg';
import enjoy from './components/images/enjoy.svg';
import {NavLink} from 'react-router-dom';
import Button from '@material-ui/core/Button';


const Instructions = (props) => {
    
    return <div className="instructionsContainer" >
        <h1 id="HowItWorks">How it Works  </h1>
        <div className="instructionsInfo">

        <div>
        <img  src={userPass}></img>
        <p>Register or sign in to Pantry Buddy.</p>
        <NavLink 
        exact
        to={'/login'}
        >
        <Button  size="small" style={{backgroundColor: "#F20530", marginTop: "1em"  }} variant="contained" color="primary">Login</Button>
        
        </NavLink>
        </div>

        <div>
        <img  src={SearchRecipes}></img>
        <p >Add ingredients to your pantry.Once you're done adding ingredients, search for recipes based on the ingredients in your pantry.</p>
        <NavLink 
        exact
        to={'/search'}
        >
        <Button  size="small" style={{backgroundColor: "#B9BF04", marginTop: "1em"  }} variant="contained" color="primary">Search Recipes</Button>
        
        </NavLink>
        </div>

        <div>
        <img  src={savedPantriesAndRecipes}></img>
        <p>If you like a recipe, you can save it to Your Recipes. You can also save a list of ingredients to Your Pantries</p>
        <NavLink 
        exact
        to={'/pantries'}
        >
        <Button  size="small" style={{backgroundColor: "#F23D6D", marginTop: "1em"  }} variant="contained" color="primary">Your Pantries</Button>
        </NavLink>

        <NavLink 
        exact
        to={'/recipes'}
        >
        <Button  size="small" style={{backgroundColor: "#F2B705", marginLeft: "1em", marginTop: "1em"  }} variant="contained" color="primary">Your Recipes</Button>
        </NavLink>
        </div>

        <div>
        <img  src={enjoy}></img>
        <p>Enjoy a delicious home-cooked meal with ingredients already in your pantry</p>
        <NavLink 
        exact
        to={'/login'}
        >
        
        </NavLink>
        </div>

        </div>
      
    </div>
}

export default Instructions