import React from "react";
import './App.css';
import Input from './components/Input.js'
import InputButton from './components/InputButton.js'
import Ingredient from './components/Ingredient.js'
import RecipeList from './components/RecipeList.js'
import {NavLink} from 'react-router-dom'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';

import axios from 'axios';
import { URL } from './url';

class Home extends React.Component {
//pantry list will be in the db and then it will be displaced to state.
  state = {recipeTitle: 'Fetching...',
  recipeIngredients: [],
  input: '',
  todoInput: '',
  pantryNameinput: '',
  
  vegan: false,
  vegetarian: false,
  glutenFree: false,
  dairyFree: false,
  keto: false,
  NotLoggedIn: false,
ingredients: [],
unusedIngredients: ``,
recipes: [],
pantrySubmit: false,
pantrySubmitAlert: '',
pantrySubmitSuccess: null,
};

handleInput = (e) => {
  this.setState({input: e.target.value});
}

handleTodo = (e) => {
  this.setState({todoInput: e.target.value});
}

handlePantryInput = (e) => {
  this.setState({pantryNameinput: e.target.value});
}

toggleVegan = (e) => {
  this.setState({vegan: !this.state.vegan});
}

toggleVegetarian = (e) => {
  this.setState({vegetarian: !this.state.vegetarian});
}

toggleKeto = (e) => {
  this.setState({keto: !this.state.keto});
}

toggleGlutenFree = (e) => {
  this.setState({glutenFree: !this.state.glutenFree});
}

toggleDairyFree = (e) => {
  this.setState({dairyFree: !this.state.dairyFree});
}


submitInput = (e) => {
  let self =this;
  let tempmatchArray = this.state.ingredients;
  tempmatchArray.push(this.state.input)
 
  self.setState({ingredients: [...tempmatchArray]});
 // console.log(`ingredients by user ==>`, this.state.ingredients);
  self.setState({input:``})

 //this.APICall();
}

submitChangedPantry = index => {
  const newIngredients = [...this.state.ingredients];
  newIngredients[index] =this.state.todoInput;
  this.setState({ingredients: newIngredients})
};

submitPantry = (e) => {
  this.setState({pantrySubmit: !this.state.pantrySubmit });
  
}

 savePantry = async (e) => {
   
  e.preventDefault();
  try {

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    const response = await axios.post(`${URL}/pantries/add`, {
      email: this.props.email[0],
      pantryName: this.state.pantryNameinput,
      ingredients: this.state.ingredients,
      dateCreated: date
    });
    //setMessage(response.data.message);
    // console.log(response)
    if (response.data.ok) {
      this.setState({pantrySubmitSuccess: true})
      //console.log(this.state.ingredients)
      setTimeout(() => {
        this.props.history.push('/pantries');
      }, 2000);
    }
  } catch (error) {
    console.log(error);
  }
};

toggleSubmitPantrySuccess =() => {
  this.setState({ pantrySubmitSuccess: !this.state.pantrySubmitSuccess})
}


submitInputbyIngredient = (e) => {
  let self =this;
  let tempmatchArray = this.state.ingredients;
  tempmatchArray.push(e)
  // tempIngredients.forEach(function(obj, index){
    
  // });
  self.setState({ingredients: [...tempmatchArray]});
  self.setState({input:``})

 //this.APICall();
}
  showCategoryItems = () => { 

    let  renderItems =(arr) => (
   
      this.state.matchingArray.map( (item, idx) => {
        return <div key={idx}>{item}</div>
      })
      )
    }

    clearAll =() => {
    this.setState({ingredients: []})
    }

  
    APICall =() => {
      let self=this;
      let appID='48ba796f';
      let key =`1e6f8cea5a2aa2b8bdcaa1a3e0544682`;
      let vegan, vegetarian, glutenFree, dairyFree, keto;
      this.state.vegan? vegan='&health=vegan': vegan='';
      this.state.vegetarian? vegetarian='&health=vegetarian': vegetarian='';
      this.state.glutenFree? glutenFree='&health=gluten-free': glutenFree='';
      this.state.dairyFree? dairyFree='&health=dairy-free': dairyFree='';
      this.state.keto? keto='&diet=low-carb': keto='';
      
      let combinedIngredients = self.state.ingredients.join(`,`)

       fetch(`https://api.edamam.com/search?q=${combinedIngredients}&app_id=${appID}&app_key=${key}&from=0&to=8${vegan}${vegetarian}${keto}`)
       .then(res =>res.json())
       .then( response => {
    
         self.setState({recipes: response.hits});
  

  
       }) .catch(error => console.log(error)) 
     }
  
    // componentWillMount() {
    
    
      removeTodo = index => {
        const newIngredients = [...this.state.ingredients];
        newIngredients.splice(index, 1);
        this.setState({ingredients: newIngredients})
      };

      changeTodo = index => {
        const newIngredients = [...this.state.ingredients];
        newIngredients.splice(index, 1);
        this.setState({ingredients: newIngredients})
      };
    

  componentDidMount() {
    let id=``;
    let appID='48ba796f';
    let key =`1e6f8cea5a2aa2b8bdcaa1a3e0544682`;
    console.log(`Recipe Ingredients from component did mount`, this.state.ingredients)

  }


    render() {
      // console.log(`HOME PROPS`, this.props)
      let  renderItems =(arr) => (
     
        [...arr].map( (item, idx) => {
          return <div key={idx}>{item}</div>
        })
      )

      
      return (
        <div>
        <div className="mainDivs">
          <div className='ingredientsTab' >
            <h2>What do you have?</h2>
            <div className='inputField'>
              <TextField
          id="standard-full-width"
          label="Type in an ingredient Here"
          size="small"
          onChange={this.handleInput}
          style={{ margin: 0 }}
          placeholder="milk"
          helperText="ingredients will be added to your pantry on right."
          fullWidth
          value={this.state.input}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
              <Button onClick={this.submitInput} size="small" style={{ height: 40, backgroundcolor:"#B9BF04" }} variant="contained" color="secondary">
              
        Add new
      </Button>

            </div>
         


        <div className="buttonGroup">
       <ButtonGroup size="small" aria-label="small outlined button group">
       <Button onClick={this.toggleVegan} size="small" 
       style={
         this.state.vegan? {border: "1px solid #F23D6D", color: "white", backgroundColor: "#F23D6D"  } : {border: "1px solid #F23D6D", color: "#F23D6D"  }  
      }
       variant={this.state.vegan? "contained" : "outlined"} >Vegan</Button>
       <Button onClick={this.toggleGlutenFree} size="small" 
        style={
          this.state.glutenFree? {border: "1px solid #F23D6D", color: "white", backgroundColor: "#d22855"  } : {border: "1px solid #F23D6D", color: "#d22855"  }
        }
       variant={this.state.glutenFree? "contained" : "outlined"} >Gluten-Free</Button>
       <Button onClick={this.toggleDairyFree} 
       style={
        this.state.dairyFree? {border: "1px solid #F23D6D", color: "white", backgroundColor: "#F23D6D"  } : {border: "1px solid #F23D6D", color: "#F23D6D"  }  
        }

       size="small" variant={this.state.dairyFree? "contained" : "outlined"} color="primary">Dairy-Free</Button>

       <Button onClick={this.toggleKeto} 
        style={
          this.state.keto? {border: "1px solid #F23D6D", color: "white", backgroundColor: "#d22855"  } : {border: "1px solid #F23D6D", color: "#d22855"  }
        } 

       size="small" variant={this.state.keto? "contained" : "outlined"} color="primary">Keto</Button>
       <Button onClick={this.toggleVegetarian} size="small" 
       style={
        this.state.vegetarian? {border: "1px solid #F23D6D", color: "white", backgroundColor: "#F23D6D"  } : {border: "1px solid #F23D6D", color: "#F23D6D"  }  
        }
       variant={this.state.vegetarian? "contained" : "outlined"} color="primary">Vegetarian</Button>
       <Button onClick={this.APICall} size="small" 
        style={{ backgroundColor: '#B9BF04', }} 
       variant="contained" color="primary">Search Recipes</Button>
       </ButtonGroup> 
       </div> 
       
            </div>
            <div className='pantry'>
            <h2>Your Pantry</h2>
            <Button onClick={this.clearAll} size="large" variant="outlined" color="secondary">Clear all Ingredients!</Button>
            <Button onClick={this.submitPantry} size="large" variant="outlined" style={{border: "1px solid #F23D6D", color: "#F23D6D"  }} color="primary">Save Pantry!</Button>
           {this.state.pantrySubmit? <div class="pantrySubmit">
           <TextField
          id="standard-full-width"
          label="Name of Pantry"
          size="small"
          onChange={this.handlePantryInput}
          style={{ margin: 0 }}
          placeholder="last weeks leftover...."
          
          fullWidth
          value={this.state.pantryNameinput}
          margin="normal"
          // variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <div><Button onClick={this.props.loggedIn? this.savePantry : this.toggleSubmitPantrySuccess }  style={{backgroundColor: "#F23D6D", height: 42}} variant="contained" color="primary">Save!</Button>
        <Button onClick={this.submitPantry}  style={{backgroundColor: "#B9BF04", height: 42, marginLeft: ".5em"}} variant="contained" color="primary">X</Button></div>
        {this.state.pantrySubmitSuccess && this.props.loggedIn ? <div>Pantry saved!</div> : this.state.pantrySubmitSuccess? <div className="error">Please login</div> : null}
        </div>: null
        }
            {this.state.ingredients.map((todo, index) => (
            <Ingredient
              key={index}
              index={index}
              todo={todo}
              removeTodo={this.removeTodo}
              handleTodo={this.handleTodo}
              submitPantryChange={this.submitChangedPantry}
            />
          ))}
           
          </div>
            
         
         
        </div>
        <RecipeList submitInput={this.submitInputbyIngredient} ingredients={this.state.ingredients} products={this.state.recipes} loggedIn={this.props.loggedIn} email={this.props.email} />
        </div>
        // </NavLink>
      )
    }

}

export default Home;