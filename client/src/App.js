import React from 'react';
import './App.css';
import Home from './Home';
import About from './About';
import Contacts from './Contacts';
import Navbar from './Navbar';
import SingleProduct from'./SingleProduct';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import axios from 'axios';
import Login from './containers/Login.js';
import Register from './containers/Register.js';
import SecretPage from './containers/SecretPage.js';
import SavedPantries from './components/SavedPantries';
import SavedRecipes from './components/SavedRecipes';

import Stripe from "./components/stripe";
import PaymentSuccess from "./containers/payment_success";
import PaymentError from "./containers/payment_error";
import {NavLink} from 'react-router-dom';
import { NavHashLink } from 'react-router-hash-link';
import { URL } from './url';
 class App extends React.Component {

  
  state = {
     isLoggedIn: false,
     email: [],
     name: [],
   }
  
   verify_token = async () => {
    const token = JSON.parse(localStorage.getItem('token'));
		if (token === null) return this.setState({isLoggedIn:false});
		try {
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post(`${URL}/users/verify_token`);
			return response.data.ok ? this.setState({isLoggedIn:true}) : this.setState({isLoggedIn:false});
		} catch (error) {
			console.log(error);
		}
  };
  
  scrollWidthOffset = (el) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -80; 
    window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' }); 
}

   componentDidMount() {
    this.verify_token();
   }

   login = (token) => {
		console.log('token ===>', token);
		localStorage.setItem('token', JSON.stringify(token));
		this.setState({isLoggedIn:true});
  };
  
  logout = () => {
		localStorage.removeItem('token');
		this.setState({isLoggedIn:false});
	};

render() {

  //console.log(`email from MAIN APP`, this.state.email)
  //console.log(`Login from Main APP`, this.state.isLoggedIn)
 
  return ( 
    <div className='App'>
      
      <Router>
        <Navbar />
        <Route exact path='/search' render={(props) => <Home year={2020} loggedIn={this.state.isLoggedIn} email={this.state.email} name={this.state.name} {...props}/>} />
        <Route exact path='/' render={(props) => <About year={2020} {...props}/>} />
        <Route exact path='/contacts' render={props => (!this.state.isLoggedIn? <Redirect to='/'/> : <Contacts {...props}/>)} />
        <Route exact path='/singleproduct/:product' component={SingleProduct} />
        <Route path='/pantries' render={(props) => (this.state.isLoggedIn===false ? <Redirect to={'/login'} /> : 
        <SavedPantries email={this.state.email[0]} name={this.state.name[0]} {...props} />)} />
         <Route path='/recipes' render={(props) => (this.state.isLoggedIn===false ? <Redirect to={'/login'} /> : 
        <SavedRecipes email={this.state.email[0]} name={this.state.name[0]} {...props} />)} />
        <Route exact path="/stripe" render={props => <Stripe {...props} />} />
        <Route exact path="/secret-page" render={props => <SecretPage {...props} name={this.state.name[0]}
        logout={this.logout}
        />} />
        <Route
        exact
        path="/payment/success"
        render={props => <PaymentSuccess {...props} />}
      />
      <Route
        exact
        path="/payment/error"
        render={props => <PaymentError {...props} />}
      />
        
        <Route
				path="/register"
				render={(props) => (this.state.isLoggedIn ? <Redirect to={'/secret-page'} /> : <Register name={this.state.name} email={this.state.email} {...props} />)}
			/>
      <Route
				path="/login"
				render={(props) => (this.state.isLoggedIn ? <Redirect to={'/pantries'} /> : <Login name={this.state.name} login={this.login} email={this.state.email} {...props} />)}
			/>
     
      <footer>
        <h3>Built by Carlos Planchart</h3>
        <NavHashLink 
        exact
        to={'/#aboutTop'}
        scroll={el => this.scrollWidthOffset(el)}
        >
        <h2>
          
          © 2020 Pantry Buddy
        </h2> 
        </NavHashLink>
        <NavLink 
        exact
        to={'/stripe'}
        >
        <h2>
          Donate €
        </h2>
        </NavLink>
        </footer>
        </Router>
    </div>
  )


  }
}

export default App;
