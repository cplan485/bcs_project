import React from "react";
import './App.css';

class App extends React.Component {

  state = {recipeTitle: 'Fetching...',
  recipe1Image: '',
  recipeTitle2: '',
  recipeID:'',
  originalURL: '',
ingredients: [],
unusedIngredients: ``,
};

  componentDidMount() {
    let id=``;
    let key =`76d2babab5a542018ed614b5e1aa89bd`;
    fetch(`https://api.spoonacular.com/recipes/findByIngredients?ingredients=shrimp,+bread&number=3&apiKey=${key}`)
    .then(res =>res.json())
    .then( response => {
      console.log(`what the f`, response)
      this.setState({recipeTitle:response[0].title});
      this.setState({recipe1Image:response[0].image});
      this.setState({recipeTitle2:response[2].title});
      this.setState({recipeID:response[0].id});
      id = response[0].id;
      this.setState({unusedIngredients:response[0].unusedIngredients})
      console.log(this.state.unusedIngredients );

      return fetch(`https://api.spoonacular.com/recipes/${this.state.recipeID}/information?includeNutrition=false&apiKey=${key}`)
    }) .then(res =>res.json())
      .then( response => {
        console.log(`products`, response)
        this.setState({originalURL:response.sourceUrl});
      })
      .catch(error => console.log(error)) 
  }

  componentWillMount() {
  
  }

    render() {

      
      return (
        <div>
          <h1>{this.state.recipeTitle}</h1>
          <img src={this.state.recipe1Image} ></img>
          <h1>{this.state.recipeTitle2}</h1>
          <h1>{this.state.originalURL}</h1>
        </div>
      )
    }

}

export default App;