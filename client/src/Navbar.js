import React from 'react'
import {NavLink} from 'react-router-dom'
import pantryLogo from './components/images/pantryLogo2.png';
import { BsFillPersonFill } from 'react-icons/bs';
import { IconContext } from "react-icons";
import { RiFridgeFill } from "react-icons/ri";
import ReactTooltip from 'react-tooltip';
import { FaClipboardList } from "react-icons/fa";
import { NavHashLink } from 'react-router-hash-link';

const scrollWidthOffset = (el) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -80; 
    window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' }); 
}


const Navbar = (props) => {

   

    return <div className='navbar'>
        
        <NavLink 
        exact
        to={'/'}
        ><img className="logo" src={pantryLogo}></img></NavLink>
        <NavHashLink 
        exact
        to={'/#HowItWorks'}
        scroll={el => scrollWidthOffset(el)}
        >How It Works</NavHashLink>

        <NavLink 
        exact
        to={'/search'}
        >Search Recipes</NavLink>

        <NavHashLink
        exact
        to={'/#contactTitle'}
        scroll={el => scrollWidthOffset(el)}
        >Contact</NavHashLink>
        
        <NavLink 
        exact
        to={'/pantries'}>
        <IconContext.Provider value={{className: "icons", size: "2em" }}>
        <RiFridgeFill data-tip="Your Saved Pantries"/><ReactTooltip /></IconContext.Provider></NavLink>

        <NavLink 
        exact
        to={'/recipes'}>
        <IconContext.Provider value={{className: "icons", size: "2em" }}>
        <FaClipboardList data-tip="Your Saved Recipes" /><ReactTooltip /></IconContext.Provider></NavLink>

        <NavLink 
        exact
        to={'/register'}>
        <IconContext.Provider value={{className: "icons", size: "2em" }}>
        <BsFillPersonFill data-tip="Register/Your Profile"/><ReactTooltip /></IconContext.Provider></NavLink>
        </div>
}

export default Navbar

