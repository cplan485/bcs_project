import React from 'react'
import axios from 'axios'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { URL} from './url';



var subject= "Here is the message: "
// const Calendar = () => (
class Contacts extends React.Component {

	constructor() {
		super()
		this.state = {
			title: "How can we help you?"
		}
	}


	handleSubmit(event) {
		
		var that = this
		event.preventDefault()
		const nameInput = event.target.elements.name
		const emailInput = event.target.elements.email
		const messageInput = event.target.elements.message
		const subject1 = that.props.subject || subject
		var data = { name: nameInput.value, email: emailInput.value, message: messageInput.value, subject: subject1 }
		// console.log("--data--", name: event.target.elements.name.value, event.target.elements.email.value, event.target.elements.message.value)

		// sending request to localhost, in production could be just /sendEmail
		axios.post(`${URL}/emails/send_email`, data)
		.then((response) => {
			nameInput.value = ""
			emailInput.value = ""
			messageInput.value = ""
			alert("Your message has been sent, thanks!")
		})
		.catch(function (error) {
			console.log(error);
		})
		console.log("--SeNd!--")
	}

	render() {
		
		return (
			<div className="contactForm">

			<h1 id="contactTitle" >{this.props.title || this.state.title}</h1>
			<form onSubmit={this.handleSubmit.bind(this)}>
	
		<TextField
          required={true}
		  name="message"
		  type="text"
          label="Please write your message"
          placeholder="Message"
		  variant="outlined"
		  multiline
          rows={6}
		  fullWidth
		  style={{backgroundColor: "#D9E884", display: "block" ,margin: "2em auto", width:"50%", color: "black",    }}
        />
		

		<TextField
          required={true}
		  name="name"
		  type="text"
          label="What is your name?"
          placeholder="Name?"
		  variant="outlined"
		  fullWidth
		  style={{backgroundColor: "#D9E884", display: "block" ,margin: "0 auto", width:"50%", color: "black",   }}
        />

		<TextField
          required={true}
		  name="email"
		  type="email"
          label="Your contact email?"
          placeholder="Email?"
		  variant="outlined"
		  fullWidth
		  style={{backgroundColor: "#D9E884", display: "block" ,margin: "2em auto", width:"50%", color: "black",   }}
        />
			<Button type="submit" style={{backgroundColor: "#F2B705", marginTop: "0em",marginBottom: "2em", width:"50%", color: "black"  }} variant="contained" color="primary">Send!</Button>
			</form>

			</div>
			)
	}
}


export default Contacts