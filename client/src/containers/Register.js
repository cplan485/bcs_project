import React, { useState } from 'react';
import axios from 'axios';
import { URL } from '../url';
import {NavLink} from 'react-router-dom';
import Button from '@material-ui/core/Button';

import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const Register = (props) => {
	const [ form, setValues ] = useState({
		email: '',
		password: '',
		password2: '',
		name: '',
		showPassword: false,
		showPassword2: false,
	});
	const [ message, setMessage ] = useState('');

	const handleChange = (e) => {
		setValues({ ...form, [e.target.name]: e.target.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			props.email.push(form.email);
			props.name.push(form.name);
			//props.email = form.email;
			//console.log(`email from Register`, props);
			const response = await axios.post(`${URL}/users/register`, {
				email: form.email,
				password: form.password,
				password2: form.password2,
				name: form.name,
			});
			setMessage(response.data.message);
			//console.log(response)
			if (response.data.ok) {
				setTimeout(() => {
					props.history.push('/login');
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};

	const handleClickShowPassword = () => {
		setValues({ ...form, showPassword: !form.showPassword });
	  };

	  const handleClickShowPassword2 = () => {
		setValues({ ...form, showPassword2: !form.showPassword2 });
	  };
	
	  const handleMouseDownPassword = (event) => {
		event.preventDefault();
	  };

	return (
		<div className="forms">
		<form onSubmit={handleSubmit} onChange={handleChange} className="form_container">
			{/* <label>Name</label>
			<input name="name" /> */}

			<TextField
          required={true}
		  name="name"
		  type="text"
          label="What is your name?"
          placeholder="name?"
		  variant="outlined"
		  fullWidth
		  style={{backgroundColor: "white", display: "block" , color: "black",   }}
        />

			{/* <label>Email</label>
			<input name="email" /> */}
			<TextField
          required={true}
		  name="email"
		  type="text"
          label="What is your email?"
          placeholder="email?"
		  variant="outlined"
		  fullWidth
		  style={{backgroundColor: "white", display: "block" ,margin: "1em auto", color: "black",   }}
        />
			{/* <label>Password</label>
			<input name="password" /> */}

			<FormControl  variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
		  style={{backgroundColor: "white", marginBottom: "1em"  }}
            id="outlined-adornment-password"
            type={form.showPassword ? 'text' : 'password'}
			value={form.password}
			name="password"
			
            // onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {form.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>

		<FormControl  variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">Repeat Password</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={form.showPassword2 ? 'text' : 'password'}
			value={form.password2}
			name="password2"
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword2}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {form.showPassword2 ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>

			{/* <label>Repeat password</label>
			<input name="password2" /> */}

			<Button onClick={handleSubmit} size="small" style={{backgroundColor: "#B9BF04", marginTop: "1em"  }} variant="contained" color="primary">Register</Button>
			<div className="message">
				<h4>{message}</h4>
			</div>

			<p>Already have an account? 
			<NavLink 
        exact
        to={'/login'}
        > Sign in here</NavLink></p>
		</form>
		</div>
	);
};

export default Register;
