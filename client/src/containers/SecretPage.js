import React from 'react';
import Button from '@material-ui/core/Button';

const SecretPage = (props) => {
	console.log('props from Secret Page', props)
	return (
		<div className="secret_page">
			<h1>Your Account</h1>
			<p>Welcome back {props.name}</p>
		
			<Button  
			onClick={() => {
				props.history.push('/');
				props.logout();
			}}
			size="small"  
			style={{backgroundColor: "#F20530", marginTop: "1em"  }} 
			variant="contained" 
			color="primary">Logout</Button>
		</div>
	);
};

export default SecretPage;
