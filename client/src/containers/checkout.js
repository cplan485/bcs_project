import React, { useState } from 'react';
import Product from "../components/product";
import axios from "axios";
import { injectStripe } from "react-stripe-elements";
import Button from '@material-ui/core/Button';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import thanks from './thanks.svg';
import excited from './excited.svg';
import surprised from './surprised.svg';
import { URL } from '../url';


const Checkout = props => {

  const [value, setValue] = React.useState(5);

  const handleChange = (event) => {
    setValue(event.target.value);

  };
  //=====================================================================================
  //=======================  CALCULATE TOTAL FUNCTION  ==================================
  //=====================================================================================
  const calculate_total = () => {
    let total = 0;
    products.forEach(ele => (total += ele.quantity * ele.amount));
    return total;
  };
  //=====================================================================================
  //=======================  CREATE CHECKOUT SESSION  ===================================
  //=====================================================================================
  const products = [
    {
      name: "Monetary Donation",
      images: [
        value < 25? thanks : value < 100? surprised : excited
      ],
      quantity: 1,
      amount: value // Keep the amount on the server to prevent customers from manipulating on client
    },

  ];

  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(
        `${URL}/payment/create-checkout-session`,
        { products }
      );
      debugger
      return response.data.ok
        ? (localStorage.setItem(
            "sessionId",
            JSON.stringify(response.data.sessionId)
          ),
          redirect(response.data.sessionId))
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };
  //=====================================================================================
  //=======================  REDIRECT TO STRIPE CHECKOUT  ===============================
  //=====================================================================================
  const redirect = sessionId => {
    debugger;
    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId
      })
      .then(function(result) {
        debugger; 
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };
  //=====================================================================================
  //=====================================================================================
  //=====================================================================================
  return (
    <div className="checkout_container">
      <div className="header">Checkout - Give a financial gift. Help fund further development of this app.</div>
      <div className="products_list">
        {products.map((item, idx) => {
          return <Product key={idx} {...item} />;
        })}
      </div>
     
      <div className="checkoutOptions"><FormControl component="fieldset">
      <RadioGroup row aria-label="position" name="position" defaultValue="top" onChange={handleChange}>
      
      {/* <Button onClick={handleChange}  value={5} size="large" style={value==5 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "3em"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "3em"  } } variant="contained" color="primary"
      >5$</Button>
      <Button onClick={handleChange}  value={15} size="large" style={value==15 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "3em"  }:{backgroundColor: "#B9BF04", marginLeft: "1em"  } } variant="contained" color="primary"
      >15$</Button>
      */}
      <FormControlLabel
      value={1}
      style={value==1 ?{backgroundColor: "#F20530", marginLeft: "1em", marginLeft: "1em", padding: "1em", borderRadius: "25%", color: "white"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "1em", borderRadius: "25%"  } }
      control={<Radio color="red" />}
      label="1 €"
      labelPlacement="top"
      />
      <FormControlLabel
      value={5}
      style={value==5 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "1em", borderRadius: "25%", color: "white"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "1em", borderRadius: "25%"  } }
      control={<Radio color="red" />}
      label="5 €"
      labelPlacement="top"
      />
      <FormControlLabel
      value={10}
      style={value==10 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "1em", borderRadius: "25%", color: "white"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "1em", borderRadius: "25%"  } }
      control={<Radio color="red" />}
      label="10 €"
      labelPlacement="top"
      />
      
      <FormControlLabel
      value={25}
      style={value==25 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "1em", borderRadius: "25%", color: "white"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "1em", borderRadius: "25%"  } }
      control={<Radio color="red" />}
      label="25 €"
      labelPlacement="top"
      />
      <FormControlLabel
      value={50}
      style={value==50 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "1em", borderRadius: "25%", color: "white"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "1em", borderRadius: "25%"  } }
      control={<Radio color="red" />}
      label="50 €"
      labelPlacement="top"
      />
      <FormControlLabel
      value={100}
      style={value==100 ?{backgroundColor: "#F20530", marginLeft: "1em", padding: "1em", borderRadius: "25%", color: "white"  }:{backgroundColor: "#B9BF04", marginLeft: "1em", padding: "1em", borderRadius: "25%"  } }
      control={<Radio color="red" />}
      label="100 €"
      labelPlacement="top"
      />
         
      </RadioGroup>
    </FormControl>
    <div className="checkoutFooter">
        <div className="total">Total : {calculate_total()} €</div>
        <Button  size="small" style={{backgroundColor: "#F20530", margin: ".5em 0"  }} variant="contained" color="primary"
        onClick={() => createCheckoutSession()}>PAY</Button>
      </div>
    
    </div>

    

    </div>
  );
};

export default injectStripe(Checkout);

const products2 = [
  {
    name: "Banana",
    images: [
      "https://res.cloudinary.com/estefanodi2009/image/upload/v1578491659/images/banana.jpg"
    ],
    quantity: 1,
    amount: .75 // Keep the amount on the server to prevent customers from manipulating on client
  },
  {
    name: "Kiwi",
    images: [
      "https://res.cloudinary.com/estefanodi2009/image/upload/v1578491659/images/kiwi.jpg"
    ],
    quantity: 1,
    amount: 20 // Keep the amount on the server to prevent customers from manipulating on client
  },
  {
    name: "Strawberry",
    images: [
      "https://res.cloudinary.com/estefanodi2009/image/upload/v1578493059/images/strawberry.jpg"
    ],
    quantity: 1,
    amount: 30 // Keep the amount on the server to prevent customers from manipulating on client
  }
];

