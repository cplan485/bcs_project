import React, { useState } from 'react';
import axios from 'axios';
import { URL } from '../url';
import {NavLink} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

//PASSWORD INPUT
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const Login = (props) => {
	const [ form, setValues ] = useState({
		email: '',
		password: '',
		name: '',
		showPassword: false,
	});

	

	const [ message, setMessage ] = useState('');
	
	const handleChange = (e) => {
		setValues({ ...form, [e.target.name]: e.target.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			
			props.email.push(form.email); 
			//console.log(`From LOGIN`,props.email)
			//console.log(`From LOGIN NAME`,props.name)
			//console.log('Testing login.js axios request ->', `${URL}/users/login` )
			const response = await axios.post(`${URL}/users/login`, {
				email: form.email,
				password: form.password
			});

			
			const NameResponse = await axios.get(`${URL}/users/`, );
			console.log("Login response data ",NameResponse.data)
		

		if (NameResponse.data) {
			NameResponse.data.forEach(obj => {
				 if (obj.email === props.email[0]) {
					 props.name.push(obj.name)
				 }
			})
		}
		

			setMessage(response.data.message);

			if (response.data.ok) {
			//	console.log(`testing props`, props)
				setTimeout(() => {
					props.login(response.data.token);
					props.history.push('/secret-page');
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};

	const handleClickShowPassword = () => {
		setValues({ ...form, showPassword: !form.showPassword });
	  };
	
	  const handleMouseDownPassword = (event) => {
		event.preventDefault();
	  };
	return (
		<form onSubmit={handleSubmit} onChange={handleChange} className="form_container">
			{/* <label>Email</label>
			<input name="email" /> */}
			<TextField
          required={true}
		  name="email"
		  type="text"
          label="What is your email?"
          placeholder="email?"
		  variant="outlined"
		  fullWidth
		  style={{backgroundColor: "white", display: "block" ,margin: "1.5em auto", color: "black",   }}
        />
			{/* <label>Password</label>
			<input name="password" /> */}

<FormControl  variant="outlined">
          <InputLabel htmlFor="outlined-adornment-password">Password?</InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={form.showPassword ? 'text' : 'password'}
			value={form.password}
			name="password"
			
            // onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {form.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl>



			<Button onClick={handleSubmit} size="small" style={{backgroundColor: "#F20530", marginTop: "1em"  }} variant="contained" color="primary">Login</Button>
			<div className="message">
				<p>New User? Register
				<NavLink 
        exact
        to={'/register'}
        > here!</NavLink>



				</p>
				<h4>{message}</h4>
			</div>
		</form>
	);
};

export default Login;
