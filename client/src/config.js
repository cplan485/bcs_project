const URL = window.location.hostname === `localhost`
    ? `http://localhost:3000`
    : `http://207.154.197.159/server`

export { URL };
