import React from 'react'
import Contacts from './Contacts.js'
import Instructions from './Instructions.js';
import Button from '@material-ui/core/Button';
import {NavLink} from 'react-router-dom'

const About = (props) => {
    
    return <div id="aboutTop">
        
        <div className="foodTitle">
        {/* <h1>Pantry Buddy (c) {props.year} </h1> */}
        <div className="hero-text">
            <h1>Create delicious meals with ingredients you already Have! </h1>
            <NavLink 
        exact
        to={'/search'}
        >
            <Button  size="small" style={{backgroundColor: "#F20530", marginTop: "1em"  }} variant="contained" color="primary">FIND RECIPES!</Button>
            </NavLink>
        </div>
        </div>
       <Instructions />
        <Contacts />
        {/* <p onClick={()=>props.history.goBack()}>go back</p> */}
    </div>
}

export default About