const express = require('express');
const app = express();
const path = require('path');

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

var PORT = process.env.PORT || 3000
    app.listen(PORT, function() {
        console.log(`Serving my master on port ${PORT}!`)
    })