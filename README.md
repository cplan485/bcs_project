# Pantry Buddy 
Pantry Buddy is a MERN Stack (MongoDB, Express, React, Node.js) recipe recommendation web App that allows users to search for recipes based on ingredients.

[Pantry Buddy Deployed Link](http://pantrybuddy.rocks/)

## **Preview**

## ![preview gif](preview.gif)


## :wrench: **Tech used in this project**

In this project, I used React for the front-end interface with components from [Material-UI](https://mui.com/material-ui/getting-started/overview/). Routes are handled through [React Router](https://reactrouter.com/). Because of the login authentication required, I used [Axios](https://github.com/axios/axios)to verify tokens for user login upon the application's initialization. When the user searches for ingredients, the [Spoonacular API](https://spoonacular.com/food-api) is called, and the list of ingredients gets input into the API call. To find potential products that matches ingredients an additional API call to the a Bar Code Lookup API offered by [rapidAPI.com](https://rapidapi.com/). A user is able to donate through the use of [Stripe](https://stripe.com/en-es).

The Backend implementing Node.js and Express utilizes a Model Viewer Controller design pattern to handle the storing and updates of the pantries, recipes, and users. Endpoints are accessed through [Express routers](https://expressjs.com/en/guide/routing.html). The Model schemas are developed with [Mongoose](https://mongoosejs.com/docs/) with [MongoDB Atlas](https://www.mongodb.com/atlas/database) employed as the actual database. Stripe payment processing and nodemailer are handled through the controllers.


## :mortar_board: **What did I learn from this project**

In this project I learned how to fully develop and integrate a back-end server with a React front-end as well as deploy to my hosting provider.



## :memo: **What do I still need to do**

1. Improve handling of cookies and local storage on deployed site.

2. Implement https on deployed website.

3. Improve site navigation.

4. Update interface and structure for the User's saved pantries section.

5. Fix functionality on dietary restrictions filter.

6. Implement additional styling and handling of grids with flexbox and css grids.

7. Make the website fully responsive.

8. Refactor the code.



## :seedling: **Getting Started with this project**

First clone the repo, then you will need 2 terminal windows.

In the first one you will run a client:

### `cd client`
### `npm install`
### `npm start`

In the second one you will run a server:
### `cd server`
### `npm install`
### `nodemon  index.js`

If you are using windows and receive a Python related
error after running npm install from the server folder,
run :

### `npm install --global --production windows-build-tools`



## :bookmark_tabs: **Usage Instructions**

Upon starting both the front-end and back-end server, you will be at the welcome screen, where you can read instructions on how to use the site. Click on the "Search Recipes" button, and on the left input a recipe item such as "milk." This item will be added to your pantry on the right, and you can continue to add more ingredients as you like. 

Once you are satisfied with the ingredients, click the "SEARCH RECIPES" button. Suggested ingredients will appear, and if you would like to save that ingredient, click on the main image. Below are the "Total Ingredients", "Missing Ingredients" Nutritional Restrictions section, Nutrition, and Original Link section. Click on the "+" button on the ingredients to see all the ingredients, and the "+" button on the Missing Ingredients to see all your missing ingredients. If you want to add that ingredient to your pantry click on the "+" button on the right, and it will be added to your pantry. To find a list of suggested products to buy from your Missing Ingredients click on the shopping cart icon.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
